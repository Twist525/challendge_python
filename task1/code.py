def arithmetic(number_one, number_two, operation):
    def print_response(summary):
        print(f'{number_one} {operation} {number_two} = {summary}')

    if operation == '+':
        summary = number_one + number_two
        return print_response(summary)

    elif operation == '-':
        summary = number_one - number_two
        return print_response(summary)

    elif operation == '*':
        summary = number_one * number_two
        return print_response(summary)

    elif operation == '/':
        summary = number_one / number_two
        return  print_response(summary)

    else:
        print("Неизвестная операция!")
