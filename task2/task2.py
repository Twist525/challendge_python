'''
https://pythonworld.ru/osnovy/tasks.html
Написать функцию is_year_leap, принимающую 1 аргумент — год, и возвращающую True, если год високосный, и False иначе.
'''


def is_year_leap(year):
    if not year % 4:
        if not year % 100:
            if not year % 400:
                return True
            return False
        return True
    return False
