"""
https://pythonworld.ru/osnovy/tasks.html
Написать функцию XOR_cipher, принимающая 2 аргумента: строку, которую нужно зашифровать,
и ключ шифрования, которая возвращает строку, зашифрованную путем применения функции XOR (^) над символами строки с ключом.
Написать также функцию XOR_uncipher, которая по зашифрованной строке и ключу восстанавливает исходную строку.
"""


def xor_cipher(string, key):
    string_hash = ''
    for i in string:
        string_hash += chr(ord(i) ^ key)
    print(f'Зашифрованное сообщение: {string_hash}')
    return string_hash


def xor_uncipher(hash_string, key):
    string_without_hash = ''
    for i in hash_string:
        string_without_hash += chr(ord(i) ^ key)
    print(f'Разшифрованное сообщение: {string_without_hash}')
    return string_without_hash


xor_uncipher(xor_cipher('hello_world', 123), 123)
